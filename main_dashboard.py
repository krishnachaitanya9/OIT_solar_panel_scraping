from bs4 import BeautifulSoup
from selenium import webdriver
import time
import re
import sys



class dashboard():
    def __init__(self):
        #self.url = "http://128.138.70.6/56E82/"
        self.url = sys.argv[1]
        #self.browser = webdriver.PhantomJS(executable_path='./bin/phantomjs')
        self.browser = webdriver.PhantomJS()
        self.browser.get(self.url)
        time.sleep(5)
        # self.scrape_values(self.url,self.browser)

    def extract_decimal_from_ID(self, soup, S):
        try:
            testHTMLString = soup.find_all(id=S)[0]
            test = re.findall("\d+.\d+", testHTMLString.text)[0]
            return test
        except:
            return 'n/a'

    @property
    def SavingsCO2(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "emmSavingsCO2")

    @property
    def MilesNotDriven(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "emmSavingsMiles")

    @property
    def TreesGrown(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "emmSavingsTrees")

    @property
    def TotalPowerProduced(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "totPowerProd")

    @property
    def TotalPowerConsumed(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "totPowerCons")

    @property
    def TotalNetPower(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "totPowerNet")

    @property
    def CurrentPerformanceProduction(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "currPerfProd")

    @property
    def CurrentPerformanceConsumption(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "currPerfCons")

    @property
    def CumulativeResultsProduced(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "solarEnergySince")

    @property
    def CumulativeResultsConsumed(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "totalEnergySince")

    @property
    def CumulativeResultsNet(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "netEnergySince")

    def __str__(self):
        return 'CO2Savings:' + self.SavingsCO2 +' '+ \
               'MilesNotDriven:' +self.MilesNotDriven +' '+ \
               'TreesGrown:' + self.TreesGrown +' '+ \
               'TotalPowerProduced:' + self.TotalPowerProduced +' '+ \
               'TotalPowerConsumed:' + self.TotalPowerConsumed +' '+ \
               'TotalNetPower:' + self.TotalNetPower +' '+ \
               'CurrentPerformanceProduction:' + self.CurrentPerformanceProduction +' '+ \
               'CurrentPerformanceConsumption:' + self.CurrentPerformanceConsumption +' '+ \
               'CumulativeResultsProduced:' + self.CumulativeResultsProduced +' '+ \
               'CumulativeResultsConsumed:' + self.CumulativeResultsConsumed +' '+ \
               'CumulativeResultsNet:' + self.CumulativeResultsNet


class channel_checker(dashboard):

    def __init__(self):
        self.url = sys.argv[1]
        #self.browser = webdriver.PhantomJS(executable_path='./bin/phantomjs')
        self.browser = webdriver.PhantomJS()
        self.browser.get(self.url)
        time.sleep(5)
        # self.scrape_values(self.url,self.browser)

    def extract_decimal_from_ID(self, soup, S):
        try:
            testHTMLString = soup.find_all(id=S)[0]
            test = re.findall("\d+.\d+", testHTMLString.text)[0]
            return test
        except:
            return 'n/a'

    @property
    def freq(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "freq")
    @property
    def L1_ch0(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch0")
    @property
    def L1_ch8(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch8")

    @property
    def L2_ch1(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch1")

    @property
    def L3_ch9(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch9")

    @property
    def L1_L2(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch0,1")

    @property
    def L2_L3(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch1,9")

    @property
    def L3_L1(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch9,0")

    @property
    def CT1(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch2")

    @property
    def CT2(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch3")

    @property
    def CT3(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch4")

    @property
    def CT4(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch5")

    @property
    def CT5(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch6")

    @property
    def CT6(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch7")

    @property
    def CT7(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch10")

    @property
    def CT8(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch11")

    @property
    def CT9(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch12")

    @property
    def CT10(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch13")

    @property
    def CT11(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch14")

    @property
    def CT12(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "ch15")

    @property
    def Solar_L1(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "value0")

    @property
    def Solar_L2(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "value1")

    @property
    def Solar_L3(self):
        self.html = self.browser.page_source
        self.soup = BeautifulSoup(self.html, 'lxml')
        return self.extract_decimal_from_ID(self.soup, "value2")

    def __str__(self):
        return 'freq:'+self.freq+' '+\
               'L1_ch0:'+self.L1_ch0+' '+\
               'L1_ch8:'+self.L1_ch8+' '+\
               'L3_ch9:'+self.L3_ch9+' '+\
               'L1_L2:'+self.L1_L2+' '+\
               'L2-L3:'+self.L2_L3+' '+\
               'CT1:'+self.CT1+' '+\
               'CT2:'+self.CT2+' '+\
               'CT3:'+self.CT3+' '+\
               'CT4:'+self.CT4+' '+\
               'CT5:'+self.CT5+' '+\
               'CT6:'+self.CT6+' '+\
               'CT7:'+self.CT7+' '+\
               'CT8:'+self.CT8+' '+\
               'CT9:'+self.CT9+' '+\
               'CT10:'+self.CT10+' '+\
               'CT11:'+self.CT11+' '+\
               'CT12:'+self.CT12+' '+\
               'Solar_L1:'+self.Solar_L1+' '+\
               'Solar_L2:'+self.Solar_L2+' '+\
               'Solar_L3:'+self.Solar_L3


if __name__ == "__main__":
    ns = sys.argv
    if "--maindashboard" in ns:
        if "--continuous" in ns:
            Dash = dashboard()
            while 1:
                print(Dash)
                time.sleep(3)
        else:
            Dash = dashboard()
            print(Dash)
    elif "--channelchecker" in ns:
        if "--continuous" in ns:
            CChecker = channel_checker()
            while 1:
                print(CChecker)
                time.sleep(3)
        else:
            CChecker = channel_checker()
            print(CChecker)

    elif "--help" in ns:
        print('For Usage, Refer below for complete set of examples')
        print('python main_dashboard.py http://128.138.70.6/56E82/ --maindashboard')
        print('python main_dashboard.py http://128.138.70.6/56E82/ --maindashboard --continuous')
        print('python main_dashboard.py http://128.138.70.6/56E82/check.html --channelchecker')
        print('python main_dashboard.py http://128.138.70.6/56E82/check.html --channelchecker --continuous')


    else:
        print('Usage not correct, Refer below for complete set of examples')
        print('python main_dashboard.py http://128.138.70.6/56E82/ --maindashboard')
        print('python main_dashboard.py http://128.138.70.6/56E82/ --maindashboard --continuous')
        print('python main_dashboard.py http://128.138.70.6/56E82/check.html --channelchecker')
        print('python main_dashboard.py http://128.138.70.6/56E82/check.html --channelchecker --continuous')



